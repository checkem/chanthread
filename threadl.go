package main

import (
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"encoding/json"
	"os"
	"strings"
	"strconv"
	"time"
	"sync"
)

type dummy struct {
	Images int
	Ext string
	Tim uint64
}

type dummy2 struct {
	Posts []dummy `json:"posts"`
}

var dImages, nImages int

func main () {

	if len(os.Args) != 2 {
		fmt.Printf("Number of arguments %d, should be 2\n",len(os.Args))
		return
	}

	urlJson := "https://a.4cdn.org/" + os.Args[1] + ".json"

	resp, err := http.Get(urlJson)
	if err != nil {
		fmt.Println(err)
		return
	}
	
	defer resp.Body.Close()
	jsonString, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	
	var tmp dummy2
	json.Unmarshal([]byte(jsonString),&tmp)
	
	tmpArg := os.Args[1]
	var board string
	if idx := strings.IndexByte(tmpArg,'/'); idx >= 0 {
		board = tmpArg[:idx]
	}else {
		fmt.Printf("Invalid input %s, aborting...",tmpArg)
		return
	}
	
	dirPath := strings.Replace(tmpArg,"/","",-1)
	if _, err = os.Stat(dirPath); os.IsNotExist(err) {
		err = os.Mkdir(dirPath,0700)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	
	fmt.Printf("Directory for images created: %s\n",dirPath)
	
	dImages = 0
	nImages = tmp.Posts[0].Images+1
	var wg sync.WaitGroup
	
	for i := range tmp.Posts {
	
		if tmp.Posts[i].Tim == 0 {
			continue;
		}
		tmpUrl := "https://i.4cdn.org/" + board + "/" + strconv.FormatUint(tmp.Posts[i].Tim,10) + tmp.Posts[i].Ext
		tmpPath := dirPath + "/" + strconv.FormatUint(tmp.Posts[i].Tim,10) + tmp.Posts[i].Ext
		
		if _, err = os.Stat(tmpPath); os.IsNotExist(err) {
			wg.Add(1)
			go DownloadImg(tmpPath,tmpUrl, &wg)
		}
		//time.Sleep(1 * time.Second)
	}
	wg.Wait()

}

func DownloadImg(path string, url string, wg *sync.WaitGroup){
	
	defer wg.Done()
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	
	out, err := os.Create(path)
	if err != nil {
		return
	}
	defer out.Close()
	
	_, err = io.Copy(out,resp.Body)
	if err != nil {
		return
	}
	elapsed := time.Since(start)
	dImages++
	fmt.Printf("Image [%d] of [%d] downloaded\n",dImages,nImages)
	fmt.Printf("%s\n%s\n",url,path)
	fmt.Printf("%s\n",elapsed)
}